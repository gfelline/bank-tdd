package ch.teko.oop.tdd

import java.time.LocalDate
import java.time.format.DateTimeFormatter

open class Clock {
    fun todayAsString(): String {
        return today().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
    }

    internal open fun today() = LocalDate.now()

}