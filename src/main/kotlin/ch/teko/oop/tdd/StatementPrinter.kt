package ch.teko.oop.tdd

import java.text.DecimalFormat
import java.util.concurrent.atomic.AtomicInteger

class StatementPrinter(val console: Console) {

    private val decimalFormatter = DecimalFormat("#.00")

    fun print(transactions:List<Transaction>) {
        console.printLine("DATE | AMOUNT | BALANCE")

        val runningBalance = AtomicInteger()

        transactions
            .map { transaction -> statementLine(transaction, runningBalance) }
            .reversed()
            .forEach { stmt -> console.printLine(stmt) }

    }

    private fun statementLine(tr: Transaction, runningBalance: AtomicInteger): String {
        return tr.date +
                " | " +
                decimalFormatter.format(tr.amount) +
                " | " +
                decimalFormatter.format(runningBalance.addAndGet(tr.amount))
    }

}
