package ch.teko.oop.tdd

class Account(
    val transactionRepository: TransactionRepository,
    val statementPrinter: StatementPrinter
) {
    fun deposit(amount: Int) {
        transactionRepository.addDeposit(amount)
    }

    fun withDraw(amount: Int) {
        transactionRepository.addWithDrawal(amount)
    }

    fun printStatement() {
        val transactions = transactionRepository.allTransactions()
        statementPrinter.print(transactions)
    }

}
