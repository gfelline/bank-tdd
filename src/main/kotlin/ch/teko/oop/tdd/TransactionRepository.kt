package ch.teko.oop.tdd

import java.util.*

class TransactionRepository(val clock: Clock) {

    val transactions = arrayListOf<Transaction>()

    fun addDeposit(amount: Int) {
        val newTransaction = Transaction(clock.todayAsString(), amount)
        transactions.add(newTransaction)
    }

    fun addWithDrawal(amount: Int) {
        val transaction = Transaction(clock.todayAsString(), -amount)
        transactions.add(transaction)
    }

    fun allTransactions(): List<Transaction> {
        return Collections.unmodifiableList(transactions)
    }

}

data class Transaction(val date:String, val amount:Int)
