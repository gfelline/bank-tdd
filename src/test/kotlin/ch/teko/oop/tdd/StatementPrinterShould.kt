package ch.teko.oop.tdd

import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import io.mockk.verifyOrder
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class StatementPrinterShould {

    @MockK(relaxUnitFun = true)
    lateinit var console: Console

    lateinit var statementPrinter:StatementPrinter

    @BeforeEach
    internal fun setUp() {
        statementPrinter = StatementPrinter(console)
    }

    @Test
    fun always_print_the_header() {

        statementPrinter.print(emptyList())

        verify { console.printLine("DATE | AMOUNT | BALANCE") }
    }

    @Test
    fun print_transaction_in_reverse_chronological_order() {
        val transactions = listOf(
            Transaction("01/04/2019", 1000),
            Transaction("02/04/2019", -100),
            Transaction("10/04/2019", 500)
        )

        statementPrinter.print(transactions)


        verifyOrder {
            console.printLine("DATE | AMOUNT | BALANCE")
            console.printLine("10/04/2019 | 500.00 | 1400.00")
            console.printLine("02/04/2019 | -100.00 | 900.00")
            console.printLine("01/04/2019 | 1000.00 | 1000.00")
        }
    }

}