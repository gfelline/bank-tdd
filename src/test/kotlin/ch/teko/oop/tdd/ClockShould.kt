package ch.teko.oop.tdd

import org.hamcrest.MatcherAssert
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.Test
import java.time.LocalDate


class ClockShould {

    @Test
    fun return_todays_date_in_dd_MM_yyyy() {
        val clock = TestableClock()

        val date = clock.todayAsString()

        MatcherAssert.assertThat(date, `is`("24/04/2019"))
    }

}

class TestableClock : Clock() {

    override fun today(): LocalDate {
        return LocalDate.of(2019, 4, 24)
    }
}


