package ch.teko.oop.tdd

import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
internal class AccountShould {

    @MockK(relaxUnitFun = true)
    lateinit var transactionRepository:TransactionRepository
    @MockK(relaxUnitFun = true)
    lateinit var statementPrinter:StatementPrinter

    lateinit var account:Account

    @BeforeEach
    internal fun setUp() {
        account = Account(transactionRepository,statementPrinter)
    }

    @Test
    internal fun store_a_deposit_transaction() {
        account.deposit(100)

        verify {
            transactionRepository.addDeposit(100)
        }

    }

    @Test
    internal fun store_a_withDraw_transaction() {
        account.withDraw(100)

        verify {
            transactionRepository.addWithDrawal(100)
        }

    }

    @Test
    internal fun print_a_statement() {
        val transactions = listOf(Transaction("", 100))
        every {
            transactionRepository.allTransactions()
        }.returns(transactions)


        account.printStatement()

        verify {
            transactionRepository.allTransactions()
            statementPrinter.print(transactions)

        }
    }
}