package ch.teko.oop.tdd

import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
internal class TransactionRepositoryShould {

    val today = "24/04/2019"

    @MockK(relaxUnitFun = true)
    private lateinit var clock: Clock

    private lateinit var transactionRepository: TransactionRepository

    @BeforeEach
    internal fun setUp() {
        transactionRepository = TransactionRepository(clock)
        every { clock.todayAsString() }.returns(today)
    }

    @Test
    fun create_and_store_a_deposit_transaction() {

        transactionRepository.addDeposit(100)
        val transations = transactionRepository.allTransactions()

        assertThat(transations.size, `is`(1))
        assertThat(transations[0], `is`(Transaction(today, 100)))
    }


    @Test
    fun create_and_store_a_withdrawal_transaction() {

        transactionRepository.addWithDrawal(100)
        val transations = transactionRepository.allTransactions()

        assertThat(transations.size, `is`(1))
        assertThat(transations[0], `is`(Transaction(today, -100)))
    }




}