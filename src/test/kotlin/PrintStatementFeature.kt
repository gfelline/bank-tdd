import ch.teko.oop.tdd.*
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verifyOrder
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
class PrintStatementFeature {

    @MockK(relaxUnitFun = true)
    lateinit var console: Console

    @MockK(relaxUnitFun = true)
    lateinit var clock: Clock


    @Test
    fun print_statement_containing_all_transaction() {
        every { clock.todayAsString() }.returnsMany("01/04/2019", "02/04/2019", "10/04/2019")

        // given
        val repo = TransactionRepository(clock)
        val statementPrinter = StatementPrinter(console)
        val account = Account(repo,statementPrinter)

        // when
        account.deposit(1000)
        account.withDraw(100)
        account.deposit(500)
        account.printStatement()

        // then
        verifyOrder {
            console.printLine("DATE | AMOUNT | BALANCE")
            console.printLine("10/04/2019 | 500.00 | 1400.00")
            console.printLine("02/04/2019 | -100.00 | 900.00")
            console.printLine("01/04/2019 | 1000.00 | 1000.00")
        }

    }
}