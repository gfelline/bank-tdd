import ch.teko.oop.tdd.*

fun main() {

    val repo = TransactionRepository(Clock())
    val statementPrinter = StatementPrinter(Console() )
    val account = Account(repo,statementPrinter)


    account.deposit(1000)
    account.withDraw(200)
    account.deposit(400)

    account.printStatement()

}